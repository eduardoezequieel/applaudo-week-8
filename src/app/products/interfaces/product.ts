export interface Product {
  id: number;
  name: string;
  price: number;
  createdAt: Date;
}

export interface Pagination {
  totalPages: number;
  itemsPerPage: number;
  totalItems: number;
  currentPage: number;
  nextPage: number;
  previousPage: number;
}

export interface Products {
  products: Product[];
  pagination: Pagination;
}
