import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SharedModule } from '../shared/shared.module';
import { ProductsService } from './services/products.service';
import { ProductComponent } from './components/product/product.component';
import { DialogContent } from './components/navbar/dialog-content';

@NgModule({
  declarations: [ProductsComponent, NavbarComponent, ProductComponent, DialogContent],
  imports: [CommonModule, ProductsRoutingModule, SharedModule, HttpClientModule],
  providers: [ProductsService],
})
export class ProductsModule {}
