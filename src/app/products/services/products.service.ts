import { api } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {} from '@auth0/angular-jwt';
import { TokenService } from 'src/app/core/token.service';

@Injectable()
export class ProductsService {
  constructor(private http: HttpClient, private tokenService: TokenService) {}

  getProducts(): Observable<any> {
    return this.http.get(api + 'products', {
      headers: this.tokenService.getAuthorization(),
    });
  }
}
