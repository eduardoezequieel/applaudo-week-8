import { Router } from '@angular/router';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { AuthService } from '../../../core/auth.service';
import { Error } from '../../interfaces/error';
import { AlertService } from 'src/app/shared/services/alert.service';
import { LoaderService } from 'src/app/core/loader.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  hide = true;

  form = this.fb.group({
    email: [
      '',
      [
        Validators.required,
        Validators.pattern(
          /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g
        ),
      ],
    ],
    password: ['', Validators.required],
  });

  constructor(
    private authService: AuthService,
    private alert: AlertService,
    private fb: UntypedFormBuilder,
    private router: Router,
    private loaderService: LoaderService
  ) {}

  submit(): void {
    this.loaderService.requestOnProgress();

    this.authService.login(this.form.value).subscribe({
      next: () => {
        this.alert.notify('success', 'Logged in successfully!').subscribe(() => {
          this.router.navigate(['products']);
        });
      },
      error: (error: Error) => {
        if (error.expected) {
          this.alert.notify('warning', error.message).subscribe({});
        } else {
          this.alert.notify('error', error.message).subscribe({});
        }
      },
      complete: () => {
        this.loaderService.finishedRequest();
      },
    });
  }
}
