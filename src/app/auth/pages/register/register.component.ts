import { UserToBeRegistered } from './../../interfaces/user';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AlertService } from 'src/app/shared/services/alert.service';
import { Error } from '../../interfaces/error';

import { AuthService } from '../../../core/auth.service';

import { PasswordValidator } from './password.validator';
import { LoaderService } from 'src/app/core/loader.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {
  hidePassword = true;
  hideConfirmPassword = true;

  form = this.fb.group(
    {
      firstName: [
        '',
        [
          Validators.required,
          Validators.pattern(/^([A-Z][a-z]+([ ]?[a-z]?['-]?[A-Z][a-z]+)*)$/),
          Validators.minLength(2),
        ],
      ],
      lastName: [
        '',
        [
          Validators.required,
          Validators.pattern(/^([A-Z][a-z]+([ ]?[a-z]?['-]?[A-Z][a-z]+)*)$/),
          Validators.minLength(2),
        ],
      ],
      username: [
        '',
        [Validators.required, Validators.pattern(/^[a-z\d](?:[a-z\d]|-(?=[a-z\d])){0,38}$/i)],
      ],
      email: [
        '',
        [
          Validators.required,
          Validators.pattern(
            /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g
          ),
        ],
      ],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['', Validators.required],
    },
    { validators: PasswordValidator.passwordDoesntMatch }
  );

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private alert: AlertService,
    private router: Router,
    private loaderService: LoaderService
  ) {}

  submit(): void {
    const value: UserToBeRegistered = {
      name: `${this.form.value.firstName!} ${this.form.value.lastName!}`,
      email: this.form.value.email!,
      password: this.form.value.password!,
      passwordConfirmation: this.form.value.confirmPassword!,
    };

    this.loaderService.requestOnProgress();

    this.authService.register(value).subscribe({
      next: () => {
        this.alert.notify('success', 'User registered successfully!').subscribe(() => {
          this.router.navigate(['login']);
        });
      },
      error: (response: Error) => {
        if (response.expected) {
          this.alert.notify('error', response.message).subscribe(() => {});
        } else {
          this.alert.notify('error', response.message).subscribe(() => {});
        }
      },
      complete: () => {
        this.loaderService.finishedRequest();
      },
    });
  }
}
