import { AbstractControl, ValidationErrors } from '@angular/forms';

export class PasswordValidator {
  static passwordDoesntMatch(control: AbstractControl): ValidationErrors | null {
    if (control.get('password')!.value != control.get('confirmPassword')!.value) {
      control.get('confirmPassword')!.setErrors({ passwordDoesntMatch: true });
      return { passwordDoesntMatch: true };
    }

    return null;
  }
}
