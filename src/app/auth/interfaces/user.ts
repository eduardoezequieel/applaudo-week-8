export interface User {
  email: string;
  password: string;
}

export interface UserToBeRegistered {
  name: string;
  email: string;
  password: string;
  passwordConfirmation: string;
}

export interface RegisteredUsed {
  id: number;
  name: string;
  email: string;
}

export interface Token {
  accessToken: string;
  refreshToken: string;
}
