import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, tap } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Token } from '../auth/interfaces/user';
import { api } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  constructor(private http: HttpClient, private jwtHelper: JwtHelperService) {}

  tokenExists(): boolean {
    let token = localStorage.getItem('token');
    let refreshToken = localStorage.getItem('refreshToken');

    if (!token || !refreshToken) {
      return false;
    }

    return true;
  }

  refreshToken(): Observable<Token> {
    const body = { refreshToken: localStorage.getItem('refreshToken') };

    return this.http.post<Token>(api + 'auth/refresh', body).pipe(
      tap((response: Token) => {
        localStorage.setItem('token', response.accessToken);
        localStorage.setItem('refreshToken', response.refreshToken);
      })
    );
  }

  getAuthorization(): HttpHeaders {
    return new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('token')!}`);
  }

  getUsernameFromToken(): string {
    return this.jwtHelper.decodeToken(localStorage.getItem('token')!).username;
  }
}
