import { tap, catchError, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserToBeRegistered, RegisteredUsed, Token, User } from '../auth/interfaces/user';
import { api } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  login(body: User): Observable<Token> {
    return this.http.post<Token>(api + 'auth/login', body).pipe(
      tap((response: Token) => {
        localStorage.setItem('token', response.accessToken);
        localStorage.setItem('refreshToken', response.refreshToken);
      }),
      catchError((error: Response) => {
        if (error.status == 401) {
          throw {
            expected: true,
            message: `Email and/or password invalid, or maybe this account doesn't exists.`,
          };
        } else {
          throw { expected: false, message: 'Something wrong happened with the server' };
        }
      })
    );
  }

  logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('refreshToken');
  }

  register(user: UserToBeRegistered): Observable<RegisteredUsed> {
    return this.http.post<RegisteredUsed>(api + 'auth/signup', user).pipe(
      catchError((error: Response) => {
        if (error.status == 400) {
          throw { expected: true, message: 'Bad request' };
        } else {
          throw { expected: false, message: 'Something wrong happened with the server' };
        }
      })
    );
  }
}
