import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoaderService {
  private loader$ = new BehaviorSubject<boolean>(false);

  get loader(): Observable<boolean> {
    return this.loader$.asObservable();
  }

  requestOnProgress(): void {
    this.loader$.next(true);
  }

  finishedRequest(): void {
    this.loader$.next(false);
  }
}
