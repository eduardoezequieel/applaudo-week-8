import { Component } from '@angular/core';
import { LoaderService } from 'src/app/core/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent {
  showLoader$ = this.loaderService.loader;

  constructor(private loaderService: LoaderService) {}
}
