import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { TokenService } from './core/token.service';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet> <app-loader></app-loader>',
  styleUrls: [],
})
export class AppComponent implements OnInit {
  constructor(private tokenService: TokenService, private router: Router) {}

  ngOnInit(): void {
    if (this.tokenService.tokenExists()) {
      this.tokenService.refreshToken().subscribe({
        error: () => {
          localStorage.removeItem('token');
          localStorage.removeItem('refreshToken');

          this.router.navigate(['auth']);
        },
      });
    }
  }
}
